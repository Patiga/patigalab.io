use image::codecs::png::PngDecoder;
use image::{ColorType, ImageDecoder, RgbaImage};
use std::rc::Rc;
use std::sync::atomic::{AtomicBool, Ordering};
use twmap::{EmbeddedImage, Image, LayerKind, LoadMultiple, TwMap, Version};
use wasm_bindgen::prelude::wasm_bindgen;
use wasm_bindgen::{JsCast, JsValue};
use wasm_bindgen_futures::JsFuture;
use web_sys::Response;
use wgpu::Backends;
use winit::dpi::{LogicalSize, PhysicalPosition, PhysicalSize};
use winit::event::{ElementState, Event, MouseButton, MouseScrollDelta, WindowEvent};
use winit::event_loop::{ControlFlow, EventLoop};
use winit::platform::web::WindowExtWebSys;
use winit::window::Window;

use twgpu::camera::Camera;
use twgpu::TwGpu;

const DEFAULT_TILES_VERTICAL: f32 = 25.;

async fn fetch_data(url: &str) -> Result<Vec<u8>, JsValue> {
    let window = web_sys::window().unwrap();
    let response = JsFuture::from(window.fetch_with_str(url)).await?;
    assert!(response.is_instance_of::<Response>());
    let response: Response = response.dyn_into()?;
    let js_array = JsFuture::from(response.array_buffer().unwrap()).await?;
    Ok(js_sys::Uint8Array::new(&js_array).to_vec())
}

async fn load_external_image(image: &mut Image, version: Version) -> Result<(), JsValue> {
    if let Image::External(ex) = image {
        let version = match version {
            Version::DDNet06 => "06",
            Version::Teeworlds07 => "07",
        };
        let url = format!("mapres_{}/{}.png", version, ex.name);
        let data = fetch_data(&url).await?;
        let image_decoder = PngDecoder::new(data.as_slice()).unwrap();
        assert_eq!(image_decoder.color_type(), ColorType::Rgba8);
        let mut image_buffer = vec![0_u8; image_decoder.total_bytes() as usize];
        let (width, height) = image_decoder.dimensions();
        image_decoder.read_image(&mut image_buffer).unwrap();
        let rgba_image = RgbaImage::from_vec(width, height, image_buffer).unwrap();
        *image = Image::Embedded(EmbeddedImage {
            name: image.name().clone(),
            image: rgba_image.into(),
        });
    }
    Ok(())
}

fn window_size() -> LogicalSize<f64> {
    let win = web_sys::window().unwrap();
    LogicalSize::new(
        win.inner_width().unwrap().as_f64().unwrap(),
        win.inner_height().unwrap().as_f64().unwrap(),
    )
}

#[wasm_bindgen(start)]
pub async fn main() -> Result<(), JsValue> {
    std::panic::set_hook(Box::new(console_error_panic_hook::hook));
    console_log::init().expect("Could not initialize logger");

    let doc = web_sys::window()
        .and_then(|win| win.document())
        .expect("No document in index.js");
    let event_loop = EventLoop::new();
    let window = winit::window::WindowBuilder::new()
        .with_inner_size(window_size())
        .build(&event_loop)
        .unwrap();
    doc.body()
        .expect("No body in document")
        .append_child(&web_sys::Element::from(window.canvas()))
        .unwrap();
    let search_params = web_sys::Url::new(&doc.url()?)?.search_params();
    let mut map = None;
    if let Some(map_name) = search_params.get("map") {
        let data = fetch_data(&format!("maps/{}", map_name)).await?;
        map = Some(TwMap::parse(&data).map_err(|err| err.to_string())?)
    }
    if let Some(url) = search_params.get("url") {
        let data = fetch_data(&url).await?;
        map = Some(TwMap::parse(&data).map_err(|err| err.to_string())?)
    }
    let mut map = map.expect("No map is specified");

    map.images.load().unwrap();
    map.groups
        .load_conditionally(|layer| layer.kind() == LayerKind::Tiles)
        .unwrap();
    futures::future::join_all(
        map.images
            .iter_mut()
            .map(|image| load_external_image(image, map.version)),
    )
    .await
    .into_iter()
    .collect::<Result<_, _>>()?;
    wasm_bindgen_futures::spawn_local(run(window, event_loop, map));
    Ok(())
}

static RESIZED: AtomicBool = AtomicBool::new(false);

async fn run(window: Window, event_loop: EventLoop<()>, map: twmap::TwMap) {
    let PhysicalSize { width, height } = window.inner_size();
    let camera = Camera::new(
        DEFAULT_TILES_VERTICAL / height as f32 * width as f32,
        DEFAULT_TILES_VERTICAL,
    );

    let mut graphics = TwGpu::init(&window, camera, &map, Backends::GL).await;
    log::info!("Adapter {:?}", graphics.gpu.adapter.limits());
    let window = Rc::new(window);
    {
        let window = window.clone();
        let closure = wasm_bindgen::closure::Closure::wrap(Box::new(move |_e: web_sys::Event| {
            let size = window_size();
            window.set_inner_size(size);
            RESIZED.store(true, Ordering::Relaxed);
            log::info!("Resizing!");
        }) as Box<dyn FnMut(_)>);
        web_sys::window()
            .unwrap()
            .add_event_listener_with_callback("resize", closure.as_ref().unchecked_ref())
            .unwrap();
        closure.forget();
    }
    let perf_timer = web_sys::window()
        .expect("Web Sys can't find a window")
        .performance()
        .expect("Web Sys window doesnt have performance");

    let mut fps = 0;
    let mut last_fps_stat = perf_timer.now();
    let mut mouse_state = ElementState::Released;
    let mut map_cursor_pos = (0., 0.);
    event_loop.run(move |event, _, control_flow| {
        *control_flow = ControlFlow::Poll;

        match event {
            Event::WindowEvent {
                event: window_event,
                ..
            } => match window_event {
                /* not supported yet by winit
                WindowEvent::Resized(size) => {
                    graphics.gpu.update_render_size(size);
                    graphics.camera.base_width =
                        DEFAULT_TILES_VERTICAL / size.height as f32 * size.width as f32;
                }
                */
                WindowEvent::CursorMoved { position, .. } => {
                    // Normalized position of cursor on screen, middle is 0, border is 0.5
                    let frac_x = (position.x as f32 / graphics.gpu.config.width as f32) - 0.5;
                    let frac_y = (position.y as f32 / graphics.gpu.config.height as f32) - 0.5;

                    // Relative map position, disregarding the camera position
                    let relative_x =
                        frac_x * graphics.camera.base_width * graphics.camera.zoom_horizontal;
                    let relative_y =
                        frac_y * graphics.camera.base_height * graphics.camera.zoom_vertical;

                    if mouse_state == ElementState::Pressed {
                        graphics.camera.x += map_cursor_pos.0 - (graphics.camera.x + relative_x);
                        graphics.camera.y += map_cursor_pos.1 - (graphics.camera.y + relative_y);
                    }

                    map_cursor_pos = (
                        graphics.camera.x + relative_x,
                        graphics.camera.y + relative_y,
                    );
                }
                WindowEvent::MouseWheel { delta, .. } => {
                    let zoom_out = match delta {
                        MouseScrollDelta::LineDelta(_, dy) => dy.is_sign_positive(),
                        MouseScrollDelta::PixelDelta(PhysicalPosition { y, .. }) => {
                            y.is_sign_positive()
                        }
                    };
                    if zoom_out {
                        graphics.camera.zoom_horizontal /= 1.1;
                        graphics.camera.zoom_vertical /= 1.1;
                    } else {
                        graphics.camera.zoom_horizontal *= 1.1;
                        graphics.camera.zoom_vertical *= 1.1;
                    }
                }
                WindowEvent::MouseInput {
                    button: MouseButton::Left,
                    state,
                    ..
                } => mouse_state = state,
                WindowEvent::CloseRequested => *control_flow = ControlFlow::Exit,
                _ => {}
            },
            Event::MainEventsCleared => {
                window.request_redraw();
            }
            Event::RedrawRequested(_) => {
                if RESIZED.swap(false, Ordering::Relaxed) {
                    let size = window.inner_size();
                    graphics.gpu.update_render_size(size);
                    graphics.camera.base_width =
                        DEFAULT_TILES_VERTICAL / size.height as f32 * size.width as f32;
                }
                // Only write fps once every 5 seconds
                if perf_timer.now() - last_fps_stat >= 5000. {
                    last_fps_stat = perf_timer.now();
                    log::info!("Fps: {}", fps as f32 / 5.);
                    fps = 0;
                }
                fps += 1;
                let time = (perf_timer.now() * 1000.) as i64;
                graphics
                    .map
                    .update_envelopes(&map, time, time, &graphics.gpu);

                graphics.render().ok();
            }
            _ => (),
        }
    });
}
